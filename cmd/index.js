'use strict';
const AWS = require('aws-sdk');
const EnvSnsTopic = process.env.SNS_TOPIC
const EnvQueue = process.env.QUEUE

exports.handler = (event) => {
    try {

        // Create publish parameters
        const params = {
            Message: JSON.stringify(event) , /* required */
            TopicArn: EnvSnsTopic,
            MessageAttributes: {
                'queue_filter': {
                    DataType: 'String', /* required */
                    StringValue: EnvQueue
                }
            },
        };

        // Create promise and SNS service object
        const publishTextPromise = new AWS.SNS({apiVersion: '2010-03-31'}).publish(params).promise();

        // Handle promise's fulfilled/rejected states
        publishTextPromise.then(data => {
            console.log("Message ", data);
            return {success:true};
        })
        .catch(err => {
            console.error(err, err.stack);
            return {success:false};
        });
    }
    catch (err) {
        console.error(err)
        return {success:false};
    }

};





